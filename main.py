import asyncio
# pip install
import websockets
import functools
import json
# pip install
import serial
import sys
import concurrent
from collections import defaultdict

DIODE_1 = bytes.fromhex('cb030630')
DIODE_2 = bytes.fromhex('cb030640')
DIODE_3 = bytes.fromhex('cb030650')
DIODE_4 = bytes.fromhex('cb030660')
DIODES = [DIODE_1, DIODE_2, DIODE_3, DIODE_4]
# TODO break file into modules


@asyncio.coroutine
def ask_exit(interval):
    while True:
        yield from asyncio.sleep(interval)


class DefaultDispatcher:

    def dispatch(self, event):
        print("dispatch() not implemented for class: " + self.getName())


class MainDispatcher(DefaultDispatcher):

    def __init__(self):
        # each callback is an object - which should be dispatcher and list of accepted event type
        # event type is the value of "type" field in messages
        self.callbacks = defaultdict(list)

    def register(self, dispatcher, accepted_types):
        for event_type in accepted_types:
            self.callbacks[dispatcher].append(event_type)

    def dispatch(self, event):
        for dispatcher in self.callbacks:
            if event["type"] in self.callbacks[dispatcher]:
                dispatcher.dispatch(event)
        if event["type"] == "error":
            print("Error!!!")
            print(event["data"])


class WsConnectionWrapper(DefaultDispatcher):
    def __init__(self, loop, dispatcher):
        self.input_queue = asyncio.Queue(loop=loop)
        self.dispatcher = dispatcher
        dispatcher.register(self, ["web_message"])

    @asyncio.coroutine
    def produce(self):
        event = yield from self.input_queue.get()
        message = json.dumps({"type": "message", "data": str(event["data"])})
        return message

    def dispatch(self, event):
        # TODO normal blocking wayt - need to use yield and in couroutine then
        self.input_queue.put_nowait(event)

    @asyncio.coroutine
    def handler(self, websocket, path):
        while True:
            try:
                listener_task = asyncio.async(websocket.recv())
                producer_task = asyncio.async(self.produce())
                done, pending = yield from asyncio.wait(
                    [listener_task, producer_task],
                    return_when=asyncio.FIRST_COMPLETED)

                if listener_task in done:
                    message = listener_task.result()
                    if message is None:
                        break
                    self.dispatcher.dispatch(json.loads(message))
                else:
                    listener_task.cancel()

                if producer_task in done:
                    message = producer_task.result()
                    if not websocket.open:
                        break
                    yield from websocket.send(message)
                else:
                    producer_task.cancel()
            except Exception as exc:
                print(exc)
                break
        print("Web Socket connection broken")


class SerialConnectionWrapper(DefaultDispatcher):

    def __init__(self, loop, dispatcher, serial_connection):
        self.loop = loop
        self.dispatcher = dispatcher
        self.serial_connection = serial_connection
        dispatcher.register(self, ["diode_control"])

    def serial_read(self):
        try:
            # blocks until timeout passed or 100 bytes read
            msg = self.serial_connection.read(100)
            return (True, msg)
        except Exception as exc:
            return (False, str(exc))

    def dispatch(self, event):
        diode_str = event['data']
        diode_hex = DIODES[int(diode_str[-1]) - 1]
        # TODO run in executor
        self.serial_connection.write(diode_hex)

    @asyncio.coroutine
    def serial_read_coroutine(self):
        with concurrent.futures.ThreadPoolExecutor(max_workers=1) as executor:
            (success, message) = yield from self.loop.run_in_executor(executor, functools.partial(self.serial_read))
            return (success, message)

    def serial_read_loop(self):
        while True:
            (success, message) = yield from self.serial_read_coroutine()
            if not success:
                self.dispatcher.dispatch({"type": "error", "data": "Serial connection error: " + message})
                break
            self.dispatcher.dispatch({"type": "web_message", "data": message})
            # may not be required - already timeout 1 on serial read
            yield from asyncio.sleep(0.1)


def main():
    if len(sys.argv) < 2:
        print("Usage main [serial _id], example COM4, /dev/ttyS4")
        return

    loop = asyncio.get_event_loop()

    main_dispatcher = MainDispatcher()
    # TODO error handling
    serial_connection = serial.Serial(sys.argv[1], 9600, timeout=1)
    serial_wrapper = SerialConnectionWrapper(loop, main_dispatcher, serial_connection)

    ws_wrapper = WsConnectionWrapper(loop, main_dispatcher)

    # TODO better cleanup and reconnect on error possibility
    web_socket_routine = websockets.serve(ws_wrapper.handler, 'localhost', 8765)

    loop.run_until_complete(web_socket_routine)
    loop.create_task(serial_wrapper.serial_read_loop())

    # TODO add web-command to stop python program
    try:
        loop.run_until_complete(asyncio.Task(ask_exit(1.0)))
    except KeyboardInterrupt:
        print('Loop stopped')
    serial_connection.close()

if __name__ == "__main__":
    main()
